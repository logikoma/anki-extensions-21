import unittest

from ..dictionary.weblio import WeblioEntry


class WeblioTestCase(unittest.TestCase):
    def test_weblio_standard(self):
        weblio_entry = WeblioEntry("holiday")
        self.assertEqual("holiday", weblio_entry.entry)
        self.assertEqual(["休日", "休み", "休業日", "長い休暇", "休暇期", "(休暇をとっての)旅行", "祝日", "祭日"],
                         weblio_entry.translations)
        self.assertEqual("https://weblio.hs.llnwd.net/e7/img/dict/kenej/audio/S-C65E22E_E-C660702.mp3",
                         weblio_entry.audio)
        example = weblio_entry.examples[0]
        self.assertEqual('<b>holiday</b> <a class="crosslink" href="https://ejje.weblio.jp/content/clothes" '
                         'title="clothesの意味">clothes</a>',
                         example.entry)
        self.assertEqual(["晴れ着."], example.translations)
        self.assertEqual("https://weblio.hs.llnwd.net/e8/audio/117a04d901aa37a32c9552d259121136.mp3?groupId"
                         "=WEBLIO_KENEJ",
                         example.audio)

    def test_weblio_no_example_audio(self):
        weblio_entry = WeblioEntry("anyway")
        self.assertEqual("anyway", weblio_entry.entry)
        self.assertEqual(["とにかく", "それにもかかわらず", "やはり", "どんな方法でも", "どのようにしても", "いいかげんに", "ぞんざいに"],
                         weblio_entry.translations)
        self.assertEqual("https://weblio.hs.llnwd.net/e7/img/dict/kenej/audio/S-9C2C0B4_E-9C2E502.mp3",
                         weblio_entry.audio)
        example = weblio_entry.examples[0]
        self.assertEqual('<b>Anyway</b>, <a class="crosslink" href="https://ejje.weblio.jp/content/let" '
                         'title="letの意味">let</a>\'<a class="crosslink" href="https://ejje.weblio.jp/content/s+have" '
                         'title="s haveの意味">s have</a> <a class="crosslink" '
                         'href="https://ejje.weblio.jp/content/fun" title="funの意味">fun</a>!',
                         example.entry)
        self.assertEqual(["とにかく楽しもう！"], example.translations)
        self.assertIsNone(example.audio)

    def test_weblio_no_audio(self):
        weblio_entry = WeblioEntry("are you sure")
        self.assertEqual("are you sure", weblio_entry.entry)
        self.assertEqual(["それは本当ですか；マジですか？", "本当ですか？；大丈夫ですか？"],
                         weblio_entry.translations)
        self.assertIsNone(weblio_entry.audio)
        example = weblio_entry.examples[0]
        self.assertEqual('<a class="crosslink" href="https://ejje.weblio.jp/content/Oh%3F" title="Oh?の英語">Oh?</a> '
                         '<b><!--AVOID_CROSSLINK-->Are<!--/AVOID_CROSSLINK--></b> '
                         '<b><!--AVOID_CROSSLINK-->you<!--/AVOID_CROSSLINK--></b> '
                         '<b><!--AVOID_CROSSLINK-->sure<!--/AVOID_CROSSLINK--></b>?', example.entry)
        self.assertEqual(["ええ? ほんと?"], example.translations)
        self.assertEqual("https://weblio.hs.llnwd.net/e8/audio/20046d7ceb1c628f004c9fad2d2ca79d.mp3?groupId"
                         "=WEBLIO_KENEJ", example.audio)

    def test_weblio_expression(self):
        weblio_entry = WeblioEntry("be used to…")
        self.assertEqual("be used to…", weblio_entry.entry)
        self.assertEqual(["なれている"], weblio_entry.translations)
        self.assertEqual("https://weblio.hs.llnwd.net/e8/audio/be+used+to%E2%80%A6.mp3", weblio_entry.audio)
        example = weblio_entry.examples[0]
        self.assertEqual('...as <a class="crosslink" href="https://ejje.weblio.jp/content/it" title="itの意味">it</a> '
                         '<b><!--AVOID_CROSSLINK-->used<!--/AVOID_CROSSLINK--></b> <a class="crosslink" '
                         'href="https://ejje.weblio.jp/content/to" title="toの意味">to</a> '
                         '<b><!--AVOID_CROSSLINK-->be<!--/AVOID_CROSSLINK--></b>', example.entry)
        self.assertEqual(["以前ほど"], example.translations)
        self.assertEqual("https://weblio.hs.llnwd.net/e8/audio/da10b37a79859e204c9b0906a153adaa.mp3?groupId"
                         "=ORG_EMAIL_SENTENCE", example.audio)

    def test_weblio_fix_entry(self):
        weblio_entry = WeblioEntry("Could . . .")
        self.assertEqual("Could...", weblio_entry.entry)
        self.assertEqual(
            ["canの過去形", "(…することが)できた", "…できる", "…してよい", "…できる(なら)", "…できるだろう", "…できただろうに", "…できるだろうに", "…したいくらいだ",
             "…しているみたいだ"], weblio_entry.translations)
        self.assertEqual("https://weblio.hs.llnwd.net/e7/img/dict/kenej/audio/S-A097A22_E-A099364.mp3",
                         weblio_entry.audio)
        example = weblio_entry.examples[0]
        self.assertEqual('You <b>could</b> <a class="crosslink" href="https://ejje.weblio.jp/content/make" '
                         'title="makeの意味">make</a> <a class="crosslink" href="https://ejje.weblio.jp/content/it." '
                         'title="it.の意味">it.</a>', example.entry)
        self.assertEqual(["出来たのに。"], example.translations)
        self.assertEqual("https://weblio.hs.llnwd.net/e8/audio/b38320a42e56e2ae02d5f907bad334ba.mp3?groupId"
                         "=ORG_EMAIL_SENTENCE", example.audio)

    def test_weblio_expression_only(self):
        weblio_entry = WeblioEntry("could have been")
        self.assertEqual("could have been", weblio_entry.entry)
        self.assertFalse(weblio_entry.translations)
        self.assertIsNone(weblio_entry.audio)
        example = weblio_entry.examples[0]
        self.assertEqual('"Nothing <b><!--AVOID_CROSSLINK-->could<!--/AVOID_CROSSLINK--></b> '
                         '<b><!--AVOID_CROSSLINK-->have<!--/AVOID_CROSSLINK--></b> '
                         '<b><!--AVOID_CROSSLINK-->been<!--/AVOID_CROSSLINK--></b> better.', example.entry)
        self.assertEqual(["「何も言うことはない。"], example.translations)
        self.assertEqual("https://weblio.hs.llnwd.net/e8/audio/3ff392560e361a6be5838cd5840d7f87.mp3?groupId"
                         "=DOTO_UCHIYAMA_scandal", example.audio)


if __name__ == '__main__':
    unittest.main()
