from typing import List


def _fix_entry(term: str) -> str:
    return term.replace(" .", ".")


class DictionaryEntry:
    def __init__(self, entry: str, translations: List[str] = [], audio: str = None):
        self._entry: str = _fix_entry(entry)
        self._translations: List[str] = translations
        self._audio: str = audio

    @property
    def entry(self) -> str:
        return self._entry

    @property
    def translations(self) -> List[str]:
        return self._translations

    @property
    def audio(self) -> str:
        return self._audio


class DictionaryEntryWithExamples(DictionaryEntry):
    def __init__(self, entry: str, translations: List[str] = [], audio: str = None):
        super().__init__(entry, translations, audio)
        self._examples: List[DictionaryEntry] = None

    @property
    def examples(self, selected_range: range = None) -> List[DictionaryEntry]:
        return self._examples
