# -*- coding: utf-8 -*-

import re
from typing import List

import requests
from bs4 import BeautifulSoup

from .dictionary import DictionaryEntryWithExamples, DictionaryEntry

WEBLIO_BASE_URL = "https://ejje.weblio.jp"
WEBLIO_WORD_URL = f"{WEBLIO_BASE_URL}/content/{{0}}"
WEBLIO_SENTENCE_URL = f"{WEBLIO_BASE_URL}/sentence/content/{{0}}"
WEBLIO_TRANSLATION_SEPARATOR = "、"


class WeblioEntry(DictionaryEntryWithExamples):
    def __init__(self, term: str):
        super().__init__(term)
        self._current_range: range = None
        self.__translations_soup: BeautifulSoup = None
        self.__examples_soup: BeautifulSoup = None

    @property
    def translations(self):
        if not self._translations:
            if not self.__translations_soup:
                sauce = requests.get(WEBLIO_WORD_URL.format(self.entry)).content
                self.__translations_soup = BeautifulSoup(sauce, "html.parser")
            # look for [主な意味] block
            translation_tag = self.__translations_soup.find("td", class_="content-explanation")
            if not translation_tag:
                # look for [訳語] block
                translation_block = self.__translations_soup.find("span", text="訳語")
                if translation_block:
                    translation_tag = translation_block.parent.a
            if translation_tag:
                self._translations = translation_tag.text.split(
                    WEBLIO_TRANSLATION_SEPARATOR)
        return self._translations

    @property
    def audio(self):
        if not self._audio:
            if not self.__translations_soup:
                sauce = requests.get(WEBLIO_WORD_URL.format(self.entry)).content
                self.__translations_soup = BeautifulSoup(sauce, "html.parser")
            audio_tag = self.__translations_soup.find("i", class_="contentTopAudioIcon")
            if audio_tag:
                self._audio = self.__translations_soup.find("i", class_="contentTopAudioIcon").audio.source["src"]
        return self._audio

    @property
    def examples(self, selected_range: range = None):
        if not self._examples or self.current_range != selected_range:
            if not self.__examples_soup:
                sauce = requests.get(WEBLIO_SENTENCE_URL.format(self.entry)).content
                self.__examples_soup = BeautifulSoup(sauce, "html.parser")
            examples_block = self.__examples_soup.find("div", class_="qotC")
            example_en = examples_block.findChild("p", class_="qotCE")
            if example_en:
                example_en.find("a", string="例文帳に追加").decompose()
                example_audio = None
                example_audio_tag = example_en.audio
                if example_audio_tag:
                    example_audio = example_audio_tag.source["src"]
                for child in example_en.find_all(re.compile("[^ab]")):
                    child.decompose()
                example_jp = examples_block.findChild("p", class_="qotCJ")
                example_jp.findChild("span").decompose()
                example = DictionaryEntry(
                    "".join(str(item) for item in example_en.contents),
                    [example_jp.text],
                    example_audio
                )
                self._examples = [example]
        return self._examples
