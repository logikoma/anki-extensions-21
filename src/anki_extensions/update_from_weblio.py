import asyncio
import os
import sys
from typing import List, Callable

from anki.collection import _Collection
from anki.lang import _
from anki.models import NoteType
from anki.notes import Note
from aqt import gui_hooks, mw
from aqt.browser import Browser, QAction, QProgressBar
from aqt.editor import Editor
from anki import stdmodels
from aqt.utils import showInfo

from .lib.dictionary.weblio import WeblioEntry

SYS_ENCODING = sys.getfilesystemencoding()
ADDON_PATH = os.path.dirname(__file__)
TEMPLATES_PATH = os.path.join(ADDON_PATH, "templates")


class ProgressTracker:
    def __init__(self, total_count: int, browser: Browser):
        self._count = 0
        self._total_count = total_count
        self._progress_bar = QProgressBar(browser)
        self._progress_bar.setGeometry(0, 0, 300, 25)
        self._progress_bar.setMaximum(self._total_count)
        self._progress_bar.show()

    def track_done(self):
        self._count += 1
        self._progress_bar.setValue(self._count)
        if self._count == self._total_count:
            self._progress_bar.hide()


def add_weblio_note_type(collection: _Collection) -> NoteType:
    models = collection.models
    model = models.new("Weblio EN/JP")
    models.addField(model, models.newField("English"))
    models.addField(model, models.newField("English audio"))
    models.addField(model, models.newField("Japanese"))
    models.addField(model, models.newField("Example"))
    models.addField(model, models.newField("Example audio"))
    models.addField(model, models.newField("Example japanese"))
    template = models.newTemplate("Recognition")
    template["qfmt"] = open(os.path.join(TEMPLATES_PATH, "weblio_question.html"), "r").read()
    template["afmt"] = open(os.path.join(TEMPLATES_PATH, "weblio_answer.html"), "r").read()
    models.addTemplate(model, template)
    models.add(model)
    return model


def update_from_weblio(note: Note):
    weblio_entry = WeblioEntry(note.fields[0])
    note.fields[0] = weblio_entry.entry
    note.fields[2] = "、".join(weblio_entry.translations[0:3])
    note.fields[1] = str(weblio_entry.audio or "")
    examples = weblio_entry.examples
    if examples:
        example = examples[0]
        note.fields[3] = example.entry
        note.fields[4] = str(example.audio or "")
        note.fields[5] = example.translations[0]
    note.flush()


async def bulk_update_from_weblio(note_ids: List[str], tracker: Callable):
    index = 0
    for note_id in note_ids:
        note = mw.col.getNote(note_id)
        if not note.model()["name"].startswith("Weblio"):
            continue
        update_from_weblio(note)
        note.flush()
        index += 1
        tracker(f"{note.fields[0]} updated from Weblio ({index}/{len(note_ids)})", index)
        await asyncio.sleep(2)


def on_update_from_weblio(editor: Editor):
    update_from_weblio(editor.note)
    editor.loadNote(True)


async def on_bulk_update_from_weblio(browser: Browser):
    selected_notes = browser.selectedNotes()
    mw.checkpoint("Bulk update from weblio")
    mw.progress.start(len(selected_notes))
    selected_notes = browser.selectedNotes()
    #progress_tracker = ProgressTracker(len(selected_notes), browser)
    await bulk_update_from_weblio(selected_notes, mw.progress.update)
    mw.progress.finish()
    mw.reset()


def add_weblio_button(buttons: List, editor: Editor):
    button = editor.addButton(icon=None, label="W", cmd="weblio", func=on_update_from_weblio)
    buttons.insert(0, str(button))


def add_weblio_menu(browser: Browser):
    a = QAction("Bulk-update from Weblio", browser)
    a.triggered.connect(lambda: asyncio.run(on_bulk_update_from_weblio(browser)))
    browser.form.menuEdit.addSeparator()
    browser.form.menuEdit.addAction(a)


stdmodels.models.append((lambda: _("Weblio EN/JP"), add_weblio_note_type))

gui_hooks.editor_did_init_buttons.append(add_weblio_button)
gui_hooks.browser_menus_did_init.append(add_weblio_menu)
